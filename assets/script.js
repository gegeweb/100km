/* @preserve
 * 100km
 * https://framagit.org/gegeweb/100km
 *
 * (c) 2020 @Sp3r4z (https://framagit.org/Sp3r4z)
 * (c) 2020 @gegeweb (https://framagit.org/gegeweb)
 *
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-later
 */

window.addEventListener('load', async () => {
    const mapBounds = L.latLngBounds( L.latLng(51.0884, 9.5597), L.latLng(41.367, -5.103600) ),
        metropole = await getJsonAsync('geojson/metropole-et-outre-mer.geojson'),
        departements = await getJsonAsync('assets/departements.json'),
        ign_attribution = '<a href="https://geoservices.ign.fr/">IGN-F/Geoportail</a>',
        ign_options = {
            minZoom: 0,
            maxZoom: 18,
            attribution: 'Carte: ' + ign_attribution,
            tileSize: 256 // les tuiles du Géooportail font 256x256px
        },
        baseLayers = [
        {
            name: 'OSM France',
            url: 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
            options: {
                attribution: 'Map: <a href="https://www.openstreetmap.fr/mentions-legales/">OpenStreetMap France</a>, Map Data &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                maxZoom: 19
            }
        },
        {
            name: 'OSM Mapnik',
            url: 'https://{s}.tile.osm.org/{z}/{x}/{y}.png',
            options: {
                attribution: 'Map: &copy <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>',
                maxZoom: 19
            }
        },
        {
            name: 'Cyclo OSM',
            url: 'https://dev.{s}.tile.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png',
            options: {
                    attribution: 'Map: <a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a>, Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a > contributors',
                maxZoom: 20
            }
        },
        {
            name:'Open Topo Map',
            url: 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png',
            options: {
                attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
                maxZoom: 19
            }
        },
        {
            name: 'IGN Standard',
            url: "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?" +
                "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
                "&STYLE=normal" +
                "&TILEMATRIXSET=PM" +
                "&FORMAT=image/jpeg" +
                "&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS" +
                "&TILEMATRIX={z}" +
                "&TILEROW={y}" +
                "&TILECOL={x}",
            options: ign_options
        },
        {
            name: 'IGN Scan Express',
            url: "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?" +
                "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
                "&STYLE=normal" +
                "&TILEMATRIXSET=PM" +
                "&FORMAT=image/jpeg" +
                "&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD" +
                "&TILEMATRIX={z}" +
                "&TILEROW={y}" +
                "&TILECOL={x}",
            options: ign_options
        },
        {
            name: 'Plan IGN',
            url: "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?" +
                "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
                "&STYLE=normal" +
                "&TILEMATRIXSET=PM" +
                "&FORMAT=image/jpeg" +
                "&LAYER=GEOGRAPHICALGRIDSYSTEMS.PLANIGN" +
                "&TILEMATRIX={z}" +
                "&TILEROW={y}" +
                "&TILECOL={x}",
            options: ign_options
        },
    ],
    overlayLayers = [
        {
            name: 'Relief',
            url: "https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png",
                options: {
                    attribution: 'Hillshading: SRTM3 v2 (<a href="http://www.nasa.gov/">NASA</a>)',
            }
        },
        {
            name: 'Unités Administratives',
            url: "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?" +
                "&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0" +
                "&STYLE=normal" +
                "&TILEMATRIXSET=PM" +
                "&FORMAT=image/png" +
                "&LAYER=ADMINEXPRESS_COG_CARTO_2018" +
                "&TILEMATRIX={z}" +
                "&TILEROW={y}" +
                "&TILECOL={x}",
            options: {
                opacity: 0.7,
                minZoom: 0,
                maxZoom: 18,
                attribution: 'Unités admin.: ' + ign_attribution,
                tileSize: 256
            }
        }
    ],
    controlLayer = L.control.layers( null, null, { collapsed: true } ),
    controlScale = L.control.scale({ imperial: false }),
    geoCoder = new L.Control.Geocoder.Nominatim({
        reverseQueryParams: {
            'accept-language': 'fr,fr-FR'
        },
        geocodingQueryParams: {
            'accept-language': 'fr,fr-FR',
            'countrycodes': 'FR'
        }
    }),
    controlGeocoder = new L.Control.Geocoder({
        defaultMarkGeocode: false,
        position: 'topleft',
        placeholder: 'Rechercher une ville, une adresse…',
        showResultIcons: true,
        iconLabel: 'Localiser une adresse',
        geocoder: geoCoder
    })
    .on('markgeocode', (e) => {
        let center = e.geocode.center,
            html = e.geocode.html || '',
            address = e.geocode.properties ? e.geocode.properties.address : null,
            dpt = null;

        if (address) dpt = getDepartementRef(address);
        createPoint(center.lat, center.lng, html, dpt);
    }),
    theMap = L.map('map');

    fitBounds();

    theMap.on('click', (e) => {
        getReverse(e.latlng.lat, e.latlng.lng);
    })
    .attributionControl.addAttribution(
        '©2020 <a href="https://framagit.org/Sp3r4z">@Sp3r4z</a> \
        <a href="https://framagit.org/gegeweb">@gegeweb</a> - \
        <a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL</a> - \
        <a href="https://framagit.org/gegeweb/100km">Source sur FramaGit</a>'
    );

    let layersArray = [],
        layerGroup = new L.layerGroup(),
        locate = -1,                        // flag pour la position
        cacheDpt = {};                      // Cache geojson département en mémoire

    layerGroup.addTo(theMap);

    baseLayers.forEach( ( e, i ) => {
        let l = new L.TileLayer(e.url, e.options);
        controlLayer.addBaseLayer(l, e.name);
        if (i === 0) theMap.addLayer(l);
    });

    overlayLayers.forEach( ( e ) => {
        let l = new L.TileLayer(e.url, e.options);
        controlLayer.addOverlay(l, e.name);
        theMap.addLayer(l);
    });

    /*
     * --------------
     * Custom Control
     * --------------
     */

    // Titre de la carte
    L.Control.mapTitle = L.Control.extend({
        options: {
            position: 'topleft'
        },
        onAdd: function () {
            let container = L.DomUtil.create('div', 'leaflet-control-title');
            container.innerHTML = '<h1>Déplacements à 100 km</h1>';
            return container;
        },
        onRemove: () => {
            // do nothing
        }
    });

    // Affichage de l'aide
    L.Control.CustomHelp = L.Control.extend({
        options: {
            position: 'bottomright'
        },
        onAdd: () => {
            let className = 'leaflet-control-customhelp',
                container = L.DomUtil.create('div', className + ' leaflet-bar leaflet-control'),
                link = L.DomUtil.create('a', className + '-button leaflet-bar-part leaflet-bar-part-single', container);
                section = L.DomUtil.create('section', className + '-section', container);
            link.title = 'Aide';
            link.role = 'button';
            link.href = '#';
            link.innerHTML = "?";
            section.innerHTML = `<h2>Comment procéder&nbsp;?</h2> \
                <ul> \
                <li>Cliquer directement sur la carte à l'endroit choisi</li>  \
                <li>🔍 Faire une recherche (ville, code postal…)</li> \
                ${(navigator.geolocation ? '<li>⦿ Localiser votre position</li>' : '')} \
                <li>♽ Réinitialiser la carte</li> \
                <li>✖️ Effacer le dernier point</li> \
                </ul> \
                <h3>Décret n°2020-548 du 11 mai 2020&nbsp;:</h3>\
                <p><em>Tout déplacement de personne la conduisant à la fois à sortir d'un périmètre défini par un rayon de 100 kilomètres de son lieu de résidence et à sortir du département dans lequel ce dernier est situé est interdit […]</em></p>`;
            link.onclick = ( e ) => {
                L.DomEvent.stopPropagation( e );
                L.DomEvent.preventDefault( e );
            }
            L.DomEvent.on(container, 'mouseenter', () => {
                L.DomUtil.addClass(container, className + '-expanded');
            });
            L.DomEvent.on(container, 'mouseleave', () => {
                L.DomUtil.removeClass(container, className + '-expanded');
            });

            return container;
        },
        onRemove: () => {
            // do nothing
        }
    });

    // Réinitialiser la carte
    L.Control.CustomClean = L.Control.extend({
        options: {
         position: 'topleft'
        },
        onAdd: () => {
            let className = 'leaflet-control-initmap',
                container = L.DomUtil.create('div',
                className + ' leaflet-bar leaflet-control'),
                link = L.DomUtil.create('a', className + '-button leaflet-bar-part leaflet-bar-part-single', container);
            link.title = 'Réinitialiser la carte';
            link.role = 'button';
            link.href = '#';
            link.innerHTML = "♽";
            link.onclick = ( e ) => {
                L.DomEvent.stopPropagation( e );
                L.DomEvent.preventDefault( e );
                clearLayer();
                fitBounds();
            }
            return container;
        },
        onRemove: () => {
            // do nothing
        }
    });

    // Suppresion du dernier point
    L.Control.CustomPoint = L.Control.extend({
        options: {
         position: 'topleft'
        },
        onAdd: function () {
            let className = 'leaflet-control-removepoint',
                container = L.DomUtil.create('div',
                className + ' leaflet-bar leaflet-control');
            let link = L.DomUtil.create('a', className + '-button leaflet-bar-part leaflet-bar-part-single', container);
            link.title = 'Supprimer le dernier point';
            link.role = 'button';
            link.href = '#';
            link.innerHTML = "✖️";
            link.onclick = ( e ) => {
                L.DomEvent.stopPropagation( e );
                L.DomEvent.preventDefault( e );
                removeLastPoint();
            }
            return container;
        },
        onRemove: () => {
            // do nothing
        }
    });

    // localiser sa position
    if (navigator.geolocation) {
        L.Control.CustomPosition = L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function () {
                let className = 'leaflet-control-locate',
                    container = L.DomUtil.create('div',
                    className + ' leaflet-bar leaflet-control');
                let link = L.DomUtil.create('a', className + '-button leaflet-bar-part leaflet-bar-part-single', container);
                link.title = 'Localiser ma position';
                link.role = 'button';
                link.href = '#';
                link.innerHTML = "⦿";
                L.DomEvent
                    .on(link, 'click', ( e ) => {
                        L.DomEvent.stopPropagation( e );
                        L.DomEvent.preventDefault( e );
                        // Ne pas recréer le layer si déjà existant et zoomer sur la position
                        if ( locate === -1 )
                            navigator.geolocation.getCurrentPosition( drawPosition );
                        else
                            fitBounds( [ layersArray[locate] ] );
                    })
                    .on(link, 'dblclick', L.DomEvent.stopPropagation);

                return container;
            },
            onRemove: () => {
                // do nothing
            }
        })
    }

    controlScale.addTo( theMap );
    controlLayer.addTo( theMap );
    controlGeocoder.addTo( theMap );
    theMap.addControl( new L.Control.mapTitle() );
    if (navigator.geolocation) theMap.addControl( new L.Control.CustomPosition() );
    theMap.addControl( new L.Control.CustomClean() );
    theMap.addControl( new L.Control.CustomPoint() );
    theMap.addControl( new L.Control.CustomHelp() );

    /*
     * ---------
     * Functions
     * ---------
     */

    async function getJsonAsync( url ) {
        return await fetch( url )
        .then( async (response) => {
            return response.ok && response.status === 200 ? await response.json() : null;
        } )
        .catch( (e) => {
            console.log( e );
            return null;
        } );
    }

    function drawPosition( p ) {
        let lat = p.coords.latitude,
            lng = p.coords.longitude;
        getReverse( lat, lng, true );
    }

    function createCircle( lat, lng ) {
        // - le cercle est un polygone
        // - la metrople est un polygone
        // On parcours les éléments en les transformant en plygone et intersection

        // https://github.com/Turfjs/turf/tree/master/packages/turf-circle
        let turfCircle = turf.circle([lng, lat], 100, { units: 'kilometers' }),
            shapeArray = [],
            metropoleElements = metropole.geometry.coordinates;

        metropoleElements.forEach( ( e ) => {
            // https://github.com/Turfjs/turf/tree/master/packages/turf-intersect
            let feat = { 'type': 'Polygon', 'coordinates': e },
                tmpCircle = turf.intersect( turfCircle, feat );
            shapeArray.push( new L.geoJSON(tmpCircle) );
        });
        return new L.FeatureGroup( shapeArray );
    }

    function createMarker( lat, lng, html ) {
        let marker = L.marker([lat, lng]);
        marker.bindPopup(`<p>${(html ? html + '<br>' : '')}<em>[ ${lat.toFixed(4)}, ${lng.toFixed(4)} ]</em></p>`).openPopup();
        return marker;
    }

    async function createPoint(lat, lng, html, departement = null) {
        let marker = createMarker(lat, lng, html),
            circle = createCircle(lat, lng),
            layers = [ marker, circle ],
            layer = null,
            geojson = null;

        if ( departement ) {
            if (!cacheDpt[departement])
                cacheDpt[departement] = new L.geoJSON( await getJsonAsync('geojson/' + departement + '.geojson') );
            geojson = cacheDpt[departement];
        }
        if ( geojson ) layers.push( geojson );
        layer = new L.FeatureGroup( layers );
        layersArray.push( layer );
        addToLayerGroup( layer );
        fitBounds( layersArray );
        return layersArray.length - 1;
    }

    function clearLayer() {
        layersArray = [];
        layerGroup.clearLayers();
        locate = -1;
    }

    function addToLayerGroup( elm ) {
        layerGroup.addLayer( elm );
    }

    function removeLastPoint() {
        if (layersArray.length > 0) {
            let i = layersArray.length - 1;
                layer = layersArray.pop();
            layerGroup.removeLayer( layer );
            fitBounds( layersArray );
            if( i === locate ) locate = -1;
        }
    }

    function getReverse( lat, lng, p = false ) {
        geoCoder.reverse({ 'lat': lat, 'lng': lng }, theMap.options.crs.scale(20), ( r ) => {
            r = r[0] || null;
            let address = r && r.properties ? r.properties.address : null,
                html = r ? ( r.html || '' ) : '',
                dpt = null;

            if ( address && address.country == 'France') {
                dpt = getDepartementRef( address );
                createPoint(lat, lng, html, dpt).then( i => {
                    if (p) locate = i;
                });
            }
        });
    }

    function getPostcodeByName( name ) {
        return departements.filter( dep => {
            return dep.nom == name;
        })[0];
    }

    function getDepartementRef( a = null ) {

        if ( !a ) return null;

        let cp = a.postcode ? a.postcode.substring(0, 2).toUpperCase() : null,
            dpt = null;

        cp = (cp == '97' || cp == '98') ? a.postcode.substring(0, 3) : cp;

        if ( !cp ) {
            if ( a.state == 'Corse' )
                cp = '20';
            else if ( a.region && a.region == 'Nouvelle-Calédonie' )
                cp = getPostcodeByName(a.region).code;
            else if ( a.county )
                cp = getPostcodeByName(a.county).code;
            else if ( a.state )
                cp = getPostcodeByName(a.state).code;
        }
        dpt = cp == '20' ? (a.county == 'Corse-du-Sud' ? '2A' : '2B') : cp;

        return dpt;
    }

    function fitBounds( l ) {
        let i = l ? l.length - 1 : -1,
            b = i > -1 ? l[i].getBounds() : mapBounds;
        theMap.fitBounds(b, {
            padding: [10, 10]
        });
    }
});
// @license-end
