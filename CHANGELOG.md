# Changelog

## [Unreleased]

## [v6.0.2] – 2020-05-19
- Don't reload and keep geojson data in memory (departments)

## [v6.0.1] - 2020-05-19

### Added 
- Zoom on the previous area after deleting a location or on France Bounds
- Zoom on the last area (circle and department) added

## [v6.0] - 2020-05-18

### Added

- Initialization of the map on France bounds
- Toggle Help Control
- Overseas Departments (DOM) and New Caledonia
- location control and layer switcher

## [v5.1] - 2020-05-12
Fork and repository cloning from [Sp3r4z/100km](https://framagit.org/Sp3r4z/100km)