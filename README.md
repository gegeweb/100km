# 100km

Adaptation du projet [100 km](https://framagit.org/Sp3r4z/100km) de [@Sp3r4z](https://framagit.org/Sp3r4z) sous licence [AGPL v3](http://www.gnu.org/licenses/agpl-3.0.html).

## À quoi ça sert ?
Tracer un cercle de 100km de rayon autour d'un point donné.

### Pourquoi ?

En France, des restrictions à la liberté de circulation ont été décretées dans le cadre de l'état d'urgence sanitaire.

Selon le [décret n° 2020-548](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000041865329&dateTexte=) du 11 mai 2020, au chapitre 2, article 3, alinéa I concernant les restrictions à la liberté de circulation :
> Tout déplacement de personne la conduisant à la fois à sortir d'un périmètre défini par un rayon de 100 kilomètres de son lieu de résidence et à sortir du département dans lequel ce dernier est situé est interdit […] 


## Limite connue
- Ça ne prend pas en compte les règles locales (zones vertes, rouges, plages…)

À l'alinéa II du décret mentionné ci-dessus :
> Le préfet de département est habilité à adopter des conditions de déplacement plus restrictives à l'intérieur d'un département lorsque les circonstances locales l'exigent.

Il convient donc de vérifier auprès des préfectures les mesures locales avant tout déplacements.

## Possibilités
- Faire une recherche avec le champ de recherche
- Avoir une zone de 100km au *clic*
- Avoir une zone de 100km autour de sa position
- Faire des unions de plusieurs zones (zones communes)

## Technologies
- [OSM France](https://www.openstreetmap.fr/)
- [OpenStreetMap](https://www.openstreetmap.org/)
- [Cyclo OSM](https://www.cyclosm.org/)
- [Géoservices IGN](https://geoservices.ign.fr/)
- [Open Topo Map](https://opentopomap.org)
- [Nominatim](https://nominatim.openstreetmap.org/)
- [Leaflet](https://leafletjs.com/)
- [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
- [France Geojson](https://github.com/gregoiredavid/france-geojson)
- [Turf](https://turfjs.org/)
